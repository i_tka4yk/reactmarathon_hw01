import {
  FETCH_CARD_LIST,
  FETCH_CARD_LIST_RESOLVE,
  FETCH_CARD_LIST_REJECT,
  REMOVE_CARD,
} from "../actions/actionTypes";

const cardListReducer = (state = {}, action) => {
  switch (action.type) {
    case FETCH_CARD_LIST:
      return {
        payload: [],
        err: null,
        isBusy: true,
      };
    case FETCH_CARD_LIST_RESOLVE:
      return {
        payload: action.payload,
        err: null,
        isBusy: false,
      };
    case FETCH_CARD_LIST_REJECT:
      return {
        payload: null,
        err: action.err,
        isBusy: false,
      };
    case REMOVE_CARD:
      const { cardId } = action;
      const { payload } = state;

      const idx = payload.findIndex((item = {}) => item.id === cardId);
      const newPayload = [
        ...(payload.slice(0, idx) || []),
        ...(payload.slice(idx + 1) || []),
      ];

      return {
        payload: newPayload,
        err: null,
        isBusy: false,
      };
    default:
      return {
        payload: state.payload || [],
        err: null,
        isBusy: false,
      };
  }
};

export default cardListReducer;
