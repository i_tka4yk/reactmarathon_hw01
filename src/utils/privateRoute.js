import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({component: Component, render: rndr, ...rest}) => {
  const user =localStorage.getItem('user');

  if (!rndr) {
    return  (
      <Route 
        {...rest}
        render={ props => user ? <Component {...props} /> : <Redirect to="/login"  />}
      />
    )
  }

  const newRndr = user ? rndr : (props) =>  <Redirect to="/login"  />

  return(
    <Route {...rest} render={newRndr} />
  )
  
}

export default PrivateRoute;