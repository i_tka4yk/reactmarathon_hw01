import React, {Component} from 'react';

import {connect} from 'react-redux';

import LoginPage from './pages/Login';
import HomePage from './pages/Home';
import {Layout, Spin, Menu} from 'antd';

import {BrowserRouter, Route, Link} from 'react-router-dom';

import PrivateRoute from './utils/privateRoute';

import s from './App.module.scss';
import FirebaseContext from './context/firebaseContext';
import {bindActionCreators} from 'redux';
import {addUserAction, removeUserAction} from './actions/userAction';

const {Header, Content} = Layout;

class App extends Component {
  componentDidMount() {
    const {auth, setUserUid} = this.context;
    const {addUser} = this.props;

    auth.onAuthStateChanged((user) => {
      if (user) {
        setUserUid(user.uid);
        localStorage.setItem('user', JSON.stringify(user.uid));
        addUser(user);
      } else {
        setUserUid(null);
        localStorage.removeItem('user');
      }
    });
  }

  handleLogout = (e) => {
    const {auth} = this.context;
    const {removeUser} = this.props;
    e.preventDefault();

    auth
      .signOut()
      .then(() => {
        localStorage.removeItem('user');
        removeUser();
      })
      .catch(function (error) {
        console.error('Sign Out error. ####: ', error);
      });
  };

  render() {
    const {userUid: user} = this.props;

    if (user === null) {
      return (
        <div className={s.loader_wrap}>
          <Spin size="large" />
        </div>
      );
    }

    return (
      <BrowserRouter>
        <Route path="/login" render={(props) => <LoginPage {...props} />} />
        <Route
          render={(props) => {
            const {
              history: {push},
            } = props;

            return (
              <Layout>
                {user ? (
                  <Header>
                    <Menu theme="dark" mode="horizontal">
                      <Menu.Item key="1">
                        <Link to="/">Home</Link>
                      </Menu.Item>
                      <Menu.Item key="2">
                        <Link to="/about">About</Link>
                      </Menu.Item>
                      <Menu.Item key="3" onClick={() => push('/contact')}>
                        Contact
                      </Menu.Item>
                    </Menu>
                  </Header>
                ) : null}
                <Content>
                  <PrivateRoute
                    path="/"
                    exact
                    render={() => (
                      <HomePage user={user} onLogout={this.handleLogout} />
                    )}
                  />
                  <PrivateRoute
                    path="/home"
                    render={() => (
                      <HomePage user={user} onLogout={this.handleLogout} />
                    )}
                  />
                  <PrivateRoute
                    path="/about"
                    render={() => <h1>Немного о себе ...</h1>}
                  />
                  <PrivateRoute
                    path="/contact"
                    render={() => <h1>Немного контактов ...</h1>}
                  />
                </Content>
              </Layout>
            );
          }}
        />
      </BrowserRouter>
    );
  }
}

App.contextType = FirebaseContext;

const mapStateToProps = (state) => ({
  userUid: state.user.userUid,
});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      addUser: addUserAction,
      removeUser: removeUserAction,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
