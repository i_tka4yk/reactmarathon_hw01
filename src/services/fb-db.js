import * as firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';

const firebaseConfig = {
  apiKey: process.env.REACT_APP_FBC_APIKEY,
  authDomain: process.env.REACT_APP_FBC_AUTHDOMAIN,
  databaseURL: process.env.REACT_APP_FBC_DATABASEURL,
  projectId: process.env.REACT_APP_FBC_PROJECTID,
  storageBucket: process.env.REACT_APP_FBC_STORAGEBUCKET,
  messagingSenderId: process.env.REACT_APP_FBC_MESSAGINGSENDERID,
  appId: process.env.REACT_APP_FBC_APPID,
};

class Firebase {
  constructor() {
    firebase.initializeApp(firebaseConfig);

    this.auth = firebase.auth();
    this.database = firebase.database();

    this.userUid = null;
  }

  setUserUid = (uid) => (this.userUid = uid);

  signWithEmail = (email, password) =>
    this.auth.signInWithEmailAndPassword(email, password);

  getUserCardsRef = (_) => {
    return this.database.ref(`/cards/${this.userUid}`);
  };

  delContent = (key) =>
    this.database.ref(`/cards/${this.userUid}/${key}`).remove();

  appendContent = (key, value, cb) =>
    this.database.ref(`/cards/${this.userUid}/${key}`).set(value, cb);
}

export default Firebase;
