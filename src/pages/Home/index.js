import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {plusAction, minusAction} from '../../actions/counterAction';
import {
  fetchCardList,
  cardListRemoveItemAction,
} from '../../actions/cardListAction';

import HeaderBlock from '../../components/HeaderBlock';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Paragraph from '../../components/Paragraph';
import CardList from '../../components/CardList';

import FirebaseContext from '../../context/firebaseContext';

class HomePage extends Component {
  state = {
    wordArr: [],
  };

  pullContent() {
    const {getUserCardsRef} = this.context;
    const {fetchCardList} = this.props;

    fetchCardList(getUserCardsRef);
  }

  componentDidMount() {
    this.pullContent();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.user !== prevProps.user) {
      this.pullContent();
    }
  }

  handleDeletedItem = ({id, key}) => {
    const {delContent} = this.context;
    const {cardListRemoveItem} = this.props;

    delContent(key).then((_) => {
      cardListRemoveItem(id);
    });
  };

  handlerAppendItem = (item) => {
    const {appendContent} = this.context;
    const {wordArr} = this.state;
    const newKey =
      wordArr.reduce((maxKey, item) => {
        return Math.max((item || {}).key, maxKey);
      }, 0) + 1;

    appendContent(newKey, item, this.pullContent.bind(this));
  };

  render() {
    const {onLogout, items, isBusy} = this.props;

    const {countNumber, plusAction, minusAction} = this.props;

    return (
      <>
        <Header>
          Header {countNumber}
          <a href="/" onClick={onLogout}>
            Log out
          </a>
        </Header>
        <button onClick={() => plusAction(1)}>PLUS</button>
        <button onClick={() => minusAction(1)}>MINUS</button>
        <HeaderBlock>
          <Paragraph>На полной скорости к новым достижениям.</Paragraph>
          <CardList
            onDeletedItem={this.handleDeletedItem}
            onAppendItem={this.handlerAppendItem}
            busy={isBusy}
            item={items}
          />
        </HeaderBlock>
        <Footer>@Footer, 2020</Footer>
      </>
    );
  }
}

HomePage.contextType = FirebaseContext;

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      plusAction,
      minusAction,
      fetchCardList,
      cardListRemoveItem: cardListRemoveItemAction,
    },
    dispatch
  );
};

const mapStateToProps = (state) => {
  const {payload, err, isBusy} = state.cardList;
  return {
    countNumber: state.counter.count,
    items: payload,
    err,
    isBusy,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
