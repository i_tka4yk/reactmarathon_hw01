import React, {Component} from 'react';
import {Layout, Form, Input, Button} from 'antd';
import s from './Login.module.scss';

import FirebaseContext from '../../context/firebaseContext';

const {Content} = Layout;

class LoginPage extends Component {
  state = {
    loginMode: true,
  };

  onLoginFinish = ({email, password}) => {
    const {auth, setUserUid} = this.context;
    const {history} = this.props;
    auth.signInWithEmailAndPassword(email, password).then((res) => {
      localStorage.setItem('user', JSON.stringify(res.user.uid));
      setUserUid(res.user.uid);
      history.push('/');
    });
  };

  onSignUpFinish = ({email, password}) => {
    const {auth} = this.context;
    auth
      .createUserWithEmailAndPassword(email, password)
      .then()
      .catch((error) => {});
  };

  onFinishFailed = (errorMsg) => {};

  onSignUp = (e) => {
    e.preventDefault();
    this.setState({loginMode: false});
  };

  renderLoginForm = () => {
    const layout = {
      labelCol: {span: 8},
      wrapperCol: {span: 18},
    };
    const tailLayout = {
      wrapperCol: {offset: 8, span: 18},
    };

    return (
      <Form
        {...layout}
        name="basic"
        initialValues={{remember: true}}
        onFinish={this.onLoginFinish}
        onFinishFailed={this.onFinishFailed}
      >
        <Form.Item
          label="Username"
          name="email"
          rules={[{required: true, message: 'Please input your email!'}]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[{required: true, message: 'Please input your password!'}]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Log in
          </Button>{' '}
          Or{' '}
          <a href="/" onClick={this.onSignUp}>
            register now!
          </a>
        </Form.Item>
      </Form>
    );
  };

  renderSignUpForm = () => {
    const layout = {
      labelCol: {span: 8},
      wrapperCol: {span: 18},
    };
    const tailLayout = {
      wrapperCol: {offset: 8, span: 18},
    };

    return (
      <Form
        {...layout}
        name="basic"
        initialValues={{remember: true}}
        onFinish={this.onSignUpFinish}
        onFinishFailed={this.onFinishFailed}
      >
        <Form.Item
          label="Username"
          name="email"
          rules={[{required: true, message: 'Please input your email!'}]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[{required: true, message: 'Please input your password!'}]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Sing Up
          </Button>
        </Form.Item>
      </Form>
    );
  };

  render() {
    const {loginMode} = this.state;

    return (
      <Layout>
        <Content>
          <div className={s.root}>
            <div className={s.form_wrap}>
              {loginMode ? this.renderLoginForm() : this.renderSignUpForm()}
            </div>
          </div>
        </Content>
      </Layout>
    );
  }
}

LoginPage.contextType = FirebaseContext;

export default LoginPage;
