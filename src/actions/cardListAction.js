import {
  FETCH_CARD_LIST,
  FETCH_CARD_LIST_RESOLVE,
  FETCH_CARD_LIST_REJECT,
  REMOVE_CARD,
} from '../actions/actionTypes';

export const fetchCardList = (getData) => {
  return (dispatch, getState) => {
    dispatch(cardListAction());
    getData()
      .once('value')
      .then((res) => {
        let wordArr = res.toJSON();
        const accum = [];
        for (let key in wordArr) {
          accum.push({
            ...wordArr[key],
            key,
          });
        }
        wordArr = accum;

        dispatch(cardListResolveAction(wordArr));
      })
      .catch((err) => {
        dispatch(cardListRejectAction(err));
      });
  };
};

export const cardListAction = () => ({
  type: FETCH_CARD_LIST,
});

export const cardListResolveAction = (payload) => ({
  type: FETCH_CARD_LIST_RESOLVE,
  payload,
});

export const cardListRejectAction = (err) => ({
  type: FETCH_CARD_LIST_REJECT,
  err,
});

export const cardListRemoveItemAction = (cardId) => ({
  type: REMOVE_CARD,
  cardId,
});
