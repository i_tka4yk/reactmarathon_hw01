import {PLUS, MINUS} from './actionTypes';

export const plusAction = (ammount) => ({
  type: PLUS,
  payload: ammount,
});

export const minusAction = (ammount) => ({
  type: MINUS,
  payload: ammount,
});
