import React, {Component} from 'react';
import {Typography} from 'antd';

const {Title} = Typography;

class Clock extends Component {
  state = {
    date: this.props.currentDate,
  };

  componentDidMount() {
    this.interval = setInterval(this.tick, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  tick = () => {
    this.setState({
      date: new Date(),
    });
  };

  render() {
    const {date} = this.state;

    return (
      <>
        <Title level={2}>Сейчас {date.toLocaleTimeString()}</Title>
      </>
    );
  }
}

export default Clock;
