import React from 'react';
import s from './Footer.module.scss';

const Footer = ({ children }) => {
  return <p className={s.footer}>{children}</p>;
};

export default Footer;