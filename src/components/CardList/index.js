import React, { Component } from "react";
import s from "./CardList.module.scss";
import Card from "../Card";

import TestContext from "../../context/testContext";

import { Input, Spin } from "antd";

import getTranslateWord from "../../services/yandex-dictionary";

const { Search } = Input;

class CardList extends Component {
  state = {
    rus: "",
    eng: "",
    value: "",
    isBusy: false,
  };

  handleRUSInputChange = (e) => {
    this.setState({
      rus: e.target.value,
    });
  };

  handleENGInputChange = (e) => {
    this.setState({
      eng: e.target.value,
    });
  };

  handleSubmitForm = (e) => {
    e.preventDefault();

    const { rus, eng } = this.state;
    const { item } = this.props;
    const maxId = item.reduce((max, item) => {
      if (item) {
        const { id } = item;
        return Math.max(max, id);
      }
      return max;
    }, 0);

    this.props.onAppendItem({
      id: maxId + 1,
      rus,
      eng,
    });

    this.setState({
      rus: "",
      eng: "",
    });
  };

  getWord = async () => {
    const { value } = this.state;

    this.setState({
      isBusy: true,
    });

    const translation = await getTranslateWord(value);
    this.setState({
      rus: translation[0].tr[0].text,
      eng: value,
      value: "",
      isBusy: false,
    });
  };

  handleSearch = async () => {
    this.setState(
      {
        isBusy: true,
      },
      this.getWord
    );
  };

  render() {
    const { item = [], onDeletedItem, busy } = this.props;
    const { value, isBusy } = this.state;

    if (busy) return <Spin size="large" />;

    return (
      <>
        <form className={s.form} onSubmit={this.handleSubmitForm}>
          RUS
          <input
            type="text"
            value={this.state.rus}
            onChange={this.handleRUSInputChange}
          />
          ENG
          <input
            type="text"
            value={this.state.eng}
            onChange={this.handleENGInputChange}
          />
          <button onClick={this.handleSubmitForm}>Add New Word</button>
        </form>
        <Search
          style={{
            margin: "10px 0",
          }}
          placeholder="input search text"
          enterButton="Search"
          size="large"
          value={value}
          loading={isBusy}
          onChange={(e) => this.setState({ value: e.target.value })}
          onSearch={this.handleSearch}
        />
        <div className={s.cardList}>
          {item.map((item) => {
            if (item) {
              const { eng, rus, id, key } = item;
              return (
                <Card
                  onDeleted={() => onDeletedItem({ id, key })}
                  key={id}
                  eng={eng}
                  rus={rus}
                />
              );
            }
            return null;
          })}
        </div>
      </>
    );
  }
}

CardList.contextType = TestContext;

export default CardList;
