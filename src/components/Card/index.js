import React from 'react';
import cl from 'classnames';
import s from './Card.module.scss';
import {CheckSquareOutlined, DeleteOutlined} from '@ant-design/icons';

class Card extends React.Component {
  state = {
    done: false,
    isRemembered: false,
  };

  handleCardClick = () => {
    this.setState(({done, isRemembered}) => {
      if (!isRemembered) {
        return {
          done: !done,
        };
      } else {
        return {};
      }
    });
  };

  handleIsRememberedClick = () => {
    this.setState(({isRemembered, done}) => {
      return {
        isRemembered: !isRemembered,
        done: !done,
      };
    });
  };

  handleDetetedClick = () => {
    this.props.onDeleted();
  };

  render() {
    const {eng, rus} = this.props;
    const {done, isRemembered} = this.state;

    return (
      <div className={s.root}>
        <div
          className={cl(s.card, {
            [s.done]: done,
            [s.isRemembered]: isRemembered,
          })}
          onClick={this.handleCardClick}
        >
          <div className={s.cardInner}>
            <div className={s.cardFront}>{eng}</div>
            <div className={s.cardBack}>{rus}</div>
          </div>
        </div>
        <div className={s.icons} onClick={this.handleIsRememberedClick}>
          <CheckSquareOutlined />
        </div>
        <div className={s.icons}>
          <DeleteOutlined onClick={this.handleDetetedClick} />
        </div>
      </div>
    );
  }
}

export default Card;
